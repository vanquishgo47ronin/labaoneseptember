using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSprite : ICommand
{
    private CommandsProcessor _commandProcessor;
    private SpriteRenderer _spriteRenderer;
    private Sprite _oldSprite;
    private Sprite _sprite;

    public ChangeSprite(SpriteRenderer spriteRender, Sprite sprite)
    {
        _spriteRenderer = spriteRender;
        _sprite = sprite;
    }

    public void Execute()
    {
        // ������ ����� ������
        _oldSprite = _spriteRenderer.sprite;
        _spriteRenderer.sprite = _sprite;
    }

    public void Undo()
    {
        // ������ ����������� ������
        _spriteRenderer.sprite = _oldSprite;
        _commandProcessor._stakChangeSprite.Clear();
    }
}
