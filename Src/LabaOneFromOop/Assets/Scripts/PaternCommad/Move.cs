using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : ICommand
{
    private CommandsProcessor _commandProcessor;

    private Transform _transformObject;
    private Vector2 _position;

    public Move(Transform obj, Vector2 vectorFromXAndY)
    {
        _transformObject = obj;
        _position = vectorFromXAndY;
    }

    public void Execute()
    {
        if (Input.GetKey(KeyCode.W))
        {
            _transformObject.Translate(_position);
            //_transformObject.TransformVector(_position);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            _transformObject.Translate(_position * -1 * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            _transformObject.Translate(Vector2.right * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            _transformObject.Translate(Vector2.left * Time.deltaTime);
        }
    }

    public void Undo()
    {
        _transformObject.position = new Vector2(0, 0);
        _commandProcessor._stakMove.Clear();
    }
}
