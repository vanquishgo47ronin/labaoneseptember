using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// ��������� ���������
public class CommandsProcessor : MonoBehaviour
{
    [SerializeField] private Transform _obj;
    [SerializeField] private SpriteRenderer _objectWithSpriteRenderer;
    [SerializeField] private Vector2 _vector2;
    [SerializeField] private Sprite _sprite1;
    [SerializeField] private Sprite _sprite2;
    [SerializeField] private Sprite _sprite3;
    private Color _color;

    public Stack<Move> _stakMove = new Stack<Move>();
    public Stack<ChangeSprite> _stakChangeSprite = new Stack<ChangeSprite>();
    public Stack<ChangeColor> _stakChangeColor = new Stack<ChangeColor>();

    // �������
    private Move _move;
    private ChangeSprite _changeSprite;
    private ChangeColor _changeColor;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            _move = new Move(_obj, _vector2);
            _move.Execute();
            _stakMove.Push(_move);
            Debug.Log(_stakMove);
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            _move = new Move(_obj, _vector2);
            _move.Execute();
            _stakMove.Push(_move);
            Debug.Log(_stakMove);
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            _move = new Move(_obj, _vector2);
            _move.Execute();
            _stakMove.Push(_move);
            Debug.Log(_stakMove);
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            _move = new Move(_obj, _vector2);
            _move.Execute();
            _stakMove.Push(_move);
            Debug.Log(_stakMove);
        }


        if (Input.GetKey(KeyCode.Keypad1))
        {
            _changeSprite = new ChangeSprite(_objectWithSpriteRenderer, _sprite1);
            _changeSprite.Execute();
            _stakChangeSprite.Push(_changeSprite);
        }
        else if (Input.GetKey(KeyCode.Keypad2))
        {
            _changeSprite = new ChangeSprite(_objectWithSpriteRenderer, _sprite2);
            _changeSprite.Execute();
            _stakChangeSprite.Push(_changeSprite);
        }
        else if (Input.GetKey(KeyCode.Keypad3))
        {
            _changeSprite = new ChangeSprite(_objectWithSpriteRenderer, _sprite3);
            _changeSprite.Execute();
            _stakChangeSprite.Push(_changeSprite);
        }


        if (Input.GetKey(KeyCode.KeypadEnter))
        {
            _color = Random.ColorHSV();
            _changeColor = new ChangeColor(_objectWithSpriteRenderer, _color);
            _stakChangeColor.Push(_changeColor);
        }
        if (Input.GetKey(KeyCode.Backspace))
        {
            // --- //
        }
    }
}
