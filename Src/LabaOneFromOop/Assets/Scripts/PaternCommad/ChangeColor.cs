using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : ICommand
{
    private CommandsProcessor _commandProcessor;
    private SpriteRenderer _spriteRenderer;
    private Color _oldColor;
    private Color _color;

    public ChangeColor(SpriteRenderer objectWithSpriteRenderer, Color newColor)
    {
        _spriteRenderer = objectWithSpriteRenderer;
        _color = newColor;
    }

    public void Execute()
    {
        _oldColor = Color.white;
        _spriteRenderer.color = _color; // Color - ����� ����
    }

    public void Undo()
    {
        // ChangeColor
        _spriteRenderer.color = _oldColor;
        _commandProcessor._stakChangeSprite.Clear();
    }
}
