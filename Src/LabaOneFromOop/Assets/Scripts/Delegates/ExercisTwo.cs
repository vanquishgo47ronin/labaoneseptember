using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class ExercisTwo : MonoBehaviour
{
    private float z = 1;

    private float x = 7.5f;
    private float y = 5.7f;

    private float g;
    // �������
    private System.Action<float, float> _actionSum;

    private delegate float DelegateOne(float x, float y);
    private delegate float DelegateTwo(float x, float y);
    private delegate float DelegateThree(float x);

    //private float sum = (float x1, float y1) => x1 + y1;
    //float sumRezul = (7.5f, 5.7f);

    ExercisTwo(float y = 75)
    {
        g = y;
        DisplayRezult(g);
    }

    void Start()
    {
        DelegateOne _delegateOne = null;
        _delegateOne = (float x, float y) => x + y;
        _ = _delegateOne(7.5f, 5.7f);
        //_delegateOne += ReturnSumRezult(7.5f, 5.7f);

        DelegateTwo _delegateTwo = null;
        _delegateTwo = (float x, float y) => x - y;
        _ = _delegateTwo(7.5f, 5.7f);
        //_delegateTwo += ReturnDifferenceRezult(7.5f, 5.7f);

        DelegateThree _delegateThree = null;
        _delegateThree = (float x) => DisplayRezult(g);
        _ = _delegateThree(g);

        _delegateOne?.Invoke(x, y);
        _delegateTwo?.Invoke(x, y);
        _delegateThree?.Invoke(g);
    }

    //private float ReturnSumRezult(float x, float y) => x + y;
    //private float ReturnDifferenceRezult(float x, float y) => x - y;

    private float DisplayRezult(float k)
    {
        for (int i = 1; i < k; i++)
        {
            z *= i;
        }
        Debug.Log(z);
        return z;
    }

    /*{
        x = 7.5f;
        y = 5.7f;
        _=(x + y) => _actionSum;
    }*/
}
