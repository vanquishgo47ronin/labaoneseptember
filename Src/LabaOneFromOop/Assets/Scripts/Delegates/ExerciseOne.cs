using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class ExerciseOne : MonoBehaviour
{
    // � �������� ��������� ����� �������.
    private delegate void DelegateOne();
    private delegate void DelegateTwo();
    private delegate void DelegateThree();

    void Start()
    {
        DelegateOne _delegateOne = null; // 1.����� ������� �������� �������� null.
        _delegateOne += DisplaysInConsoleOne; // 2. ��������� ��������.
        DelegateTwo _delegateTwo = null;
        _delegateTwo += DisplaysInConsoleTwo;
        DelegateThree _delegateThree = null;
        //_delegateThree = _delegateTwo + _delegateOne; // �����, ����� ������� ������� ��� ��������.
        _delegateOne?.Invoke();
        _delegateTwo?.Invoke();
        _delegateThree?.Invoke();

        //_delegateThree += DisplaysInConsoleThree(_delegateOne, _delegateTwo);
    }

    private void DisplaysInConsoleOne()
    {
        Debug.Log("��������");
    }

    private void DisplaysInConsoleTwo()
    {
        Debug.Log("��� �����");
    }

    private void DisplaysInConsoleThree(DelegateOne _delegateOne, DelegateTwo _delegateTwo)
    {
        // ���� ��������� ��� �������.
        Debug.Log($"{_delegateOne} {_delegateTwo}");
    }
}
