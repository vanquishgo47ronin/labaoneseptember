using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExerciThree : MonoBehaviour
{
    private string _stringNumber1 = "5";
    private string _stringNumber2 = "7";
    private string _stringNumber3 = "";

    private delegate string DelegateOne(string _stringNumber1, string _stringNumber2, string _stringNumber3);
    private delegate void DelegateTwo();

    private void Start()
    {
        DelegateOne _delegateOne = null;
        _delegateOne = (string _stringNumber1, string _stringNumber2, string _stringNumber3) => ReturnSumTwoSting(_stringNumber1, _stringNumber2, _stringNumber3);

        _delegateOne?.Invoke(_stringNumber1, _stringNumber2, _stringNumber3);
    }

    private string ReturnSumTwoSting(string str1, string str2, string str3)
    {
        str3 = str1 + str2;
        Debug.Log(str3);
        return str3;
    }
}