using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class ValueVisual : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _text;
    public delegate void DelegaidValueVisual(ObservableFloat fff);

    private float _x;
    private float _y;

    public ValueVisual(float x, float y)
    {
        _x = x;
        _y = y;
    }

    public void Init(ObservableFloat f)
    {
        DelegaidValueVisual _delegaidValueVisual = null;
        _delegaidValueVisual += UpdateView;
        _delegaidValueVisual?.Invoke(f);
    }

    public void UpdateView(ObservableFloat ff)
    {
        _text.text = $"{ff.Value.ToString()}";
    }
}
