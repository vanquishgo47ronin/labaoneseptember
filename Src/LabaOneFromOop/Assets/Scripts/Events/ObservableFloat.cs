using System.Collections;
using System.Collections.Generic;
using System.Windows.Input;
using UnityEngine;

public class ObservableFloat : MonoBehaviour
{
    public float _value;

    public ObservableFloat(float value) // �����������
    {
        _value = value;
    }

    public System.Action<float> _onValueChanged; // �������

    public virtual float Value
    {
        get
        {
            return _value;
        }
        set
        {
            _value = value;
            // �������� �������
            _onValueChanged?.Invoke(_value);
        }
    }
}

public class MinMaxFloat : ObservableFloat
{
    private float _minValue;
    private float _maxValue;

    public MinMaxFloat(float minimumValue, float maximumValue) : base(minimumValue) // �������� ����������� �������� ������
    {
        _minValue = minimumValue;
        _maxValue = maximumValue;
    }

    public override float Value
    {
        get
        {
            return _value;
        }
        set
        {
            if (_minValue < _value && _value < _maxValue)
            {
                _value = value;
            }
        }
    }

    
}