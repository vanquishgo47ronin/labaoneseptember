using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BoxWithPanel", fileName = "PanelObjects")]
public class PanelConfige : ScriptableObject
{
    public List<GameObject> Field;
}

[System.Serializable]

public class PanelObj
{
    [SerializeField] private int x = 16;
    [SerializeField] private GameObject _panelObj;
}
