using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Cell - ��� ��������
public class Cell // ������ ������ 
{
    //private int _x; // ���������� 1
    //private int _y; // ���������� 2
    private Vector2Int _position;
    private int _value;

    public System.Action<int> OnValueChanged;
    public System.Action<Vector2Int> OnPositionChanged;

    public Cell(Vector2Int positiontXandY, int value) // �����������
    {
        _position = positiontXandY;
        _value = value;
    }

    public Vector2Int VectorPosition
    {
        get { return _position; }
        set { _position = value; OnPositionChanged?.Invoke(_position); }
    }

    /*private int Y
    {
        get { return _y; }
        set { _y = value; OnPositionChanged?.Invoke(_y); }
    }*/

    public int Value
    {
        get { return _value; }
        set { _value = value; OnValueChanged?.Invoke(value); }
    }

}
