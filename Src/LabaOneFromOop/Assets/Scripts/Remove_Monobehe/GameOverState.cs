using System.Collections;
using System.Collections.Generic;
using UnityEngine.UIElements;
using UnityEngine;

public class GameOverState
{
    private Image _panel;
    private List<GameObject> _listObjects;

    public GameOverState(List<GameObject> list)
    {
        _listObjects = list;
    }

    private void Awake()
    {
        foreach (GameObject _obj in _listObjects)
        {
            _obj.SetActive(false);
        }
    }

    private void ChangePanel()
    {
        _ = _panel.tintColor.r;
    }

}
