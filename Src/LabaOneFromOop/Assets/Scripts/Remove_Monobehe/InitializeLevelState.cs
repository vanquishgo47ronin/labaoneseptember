using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitializeLevelState : IState
{
    private GameOverState _gameOverState;

    private GameObject _gameObject;

    private List<GameObject> _listWithObjects;

    public InitializeLevelState(GameObject gameobject)
    {
        _gameObject = gameobject;
    }

    void IState.Enter()
    {
        // ���������������� 100 ����� GameObejects.
        Initialization();
        _gameOverState = new GameOverState(_listWithObjects);
    }

    void IState.Exit()
    {

    }

    void IState.Update()
    {

    }

    private void Initialization()
    {
        for (int i = 0; i < 100; i++)
        {
            _listWithObjects.Add(_gameObject);
        }
    }
}
