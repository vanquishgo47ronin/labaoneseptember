using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game
{
    private StateMachine _stateMachine;
    private StatesRoot _statesRoot;

    private void Awake()
    {
        _stateMachine = new StateMachine();
        _statesRoot = new StatesRoot();
    }
}
