using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bootstrap : MonoBehaviour
{
    [SerializeField] private GameObject _cube;

    private Game _game;
    private InitializeLevelState _initializeLevelState;

    private void Awake()
    {
        _initializeLevelState = new InitializeLevelState(_cube);
        _game = new Game();
    }
}
