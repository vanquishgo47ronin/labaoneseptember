using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QueueController : MonoBehaviour
{
    [SerializeField] private ActionQueue _actionQueue;
    private float _speed = 20f;

    public void Update()
    {
        GoForvard();
        GoBack();
        GoRight();
        GoLeft();
    }

    public void GoForvard()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.position += Vector3.forward * _speed * Time.deltaTime;
            _actionQueue.AddAction();
        }
    }

    public void GoBack()
    {
        if (Input.GetKey(KeyCode.S))
        {
            transform.position += Vector3.back * _speed * Time.deltaTime;
            _actionQueue.AddAction();
        }
    }

    public void GoRight()
    {
        if (Input.GetKey(KeyCode.A))
        {
            transform.position += Vector3.right * _speed * Time.deltaTime;
            _actionQueue.AddAction();
        }
    }

    public void GoLeft()
    {
        if (Input.GetKey(KeyCode.D))
        {
            transform.position += Vector3.left * _speed * Time.deltaTime;
            _actionQueue.AddAction();
        }
    }
}
