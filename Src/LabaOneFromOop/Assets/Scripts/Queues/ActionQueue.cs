using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionQueue : MonoBehaviour
{
    //public List<System.Action> _acton = new List<System.Acton>();

    [SerializeField] private QueueController _QueueController;

    public System.Action<System.Action> _action;// �������

    public delegate void TestDeleg(); // �������

    public event TestDeleg gameEvent;

    Queue<TestDeleg> _queue = new Queue<TestDeleg>(); // ������� �� ������� |������|

    void Start()
    {
        StartCoroutine("ExaminationQueue");
    }

    public IEnumerator ExaminationQueue()
    {
        foreach (TestDeleg _deleg in _queue)
        {
            yield return new WaitForSeconds(0.1f);
            if (_queue != null)
            {
                _queue.Dequeue();
                //StopCoroutine("ExaminationQueue");
            }
        }
        AddAction();
    }

    public void AddAction()
    {
        //Debug.Log(_people.Count);
        TestDeleg _testDeleg = null;
        _testDeleg += _QueueController.Update;
        _queue.Enqueue(_testDeleg);
        Debug.Log(_queue.Count);
        //_testDeleg?.Invoke();
    }
}
