using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameField1 : MonoBehaviour
{
    [SerializeField] private GameObject _canvas;

    public int FieldSize; // ��������� ����.

    public Dictionary<Vector2Int, PipeData> _dictionry; // ������� ��� ����.

    private GameField1()
    {
        FieldSize = 3;
    }

    private void Awake()
    {
        //
    }

    private void Start()
    {
        for (int i = 0; i < FieldSize; i++)
        {
            int a = Random.Range(0, 3);
            int b = Random.Range(0, 3);
            Vector2Int vector2Int = new Vector2Int(a, b);
            CreatePipe(vector2Int);
        }
    }
    //                  // ��������� ������� �� ����.
    public void CreatePipe(Vector2Int vector2Int)
    {
        int formX = Random.Range(0, 3);
        int Rotat = Random.Range(0, 3);
        PipeData _pipeData = new PipeData(Rotat, formX);


        //CreatDictionry(vector2Int, _pipeData);
    }

    public void CreatePipeDataAddDictionry(Vector2Int vector2XY, PipeData pipeDta)
    {
        _dictionry = new Dictionary<Vector2Int, PipeData>
        {
            [vector2XY] = pipeDta, // ��������� PipaData � �������.
        };
    }

    public void CreatPrefabe()
    {
        int creatForm = Random.Range(0, 3);
        int creatRotat = Random.Range(0, 3);
        PipeData _copyPipeData = new PipeData(creatRotat, creatForm);
        // 1. ������ ��������� ������� ������ ���������.
        // 2. ������ ������ � �����������.
        // 3. � ����� ������ � �����������.
        GameObject gameObject = Instantiate(_canvas, Vector3Int.down, Quaternion.identity);
        PipeView _pipeView = gameObject.GetComponent<PipeView>();
        _pipeView.Init(_copyPipeData);
    }
}
