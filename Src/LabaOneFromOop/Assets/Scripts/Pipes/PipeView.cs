using System.Collections;
using System.Collections.Generic;
using UnityEngine.UIElements;
using UnityEngine;
using System;

public class PipeView : MonoBehaviour
{
    // �� � View �������� ������ � ������� �� ������� � unity

    [SerializeField] public Image _imagePipe;

    private PipeData _pipaData;

    public event Action EventAction;

    private void Awake()
    {
        //Init(PipeData pipaData);
    }

    public void Init(PipeData pipaData)
    {
        _pipaData = pipaData;
        _imagePipe.transform.rotation = Quaternion.Euler(0, 0, +90);
        _pipaData._action += _pipaData.Rotate;
        EventAction?.Invoke();
        Debug.Log("�������");
    }

    // �������.
    void OnMouseDown()
    {
        _imagePipe.transform.rotation = Quaternion.Euler(0, 0, +90);
    }
}
