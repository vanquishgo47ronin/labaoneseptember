using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PipeData
{
    private int _rotate;
    private int _form;

    public Action<int> _action;
    public PipeData(int rotateZ, int formX)
    {
        if (rotateZ >= 0 && rotateZ <= 3)
        {
            _rotate = rotateZ;
        }
        if (formX >= 0 && formX <= 3)
        {
            _form = formX;
        }
    }

    public int Rot
    {
        get
        {
            return _rotate;
        }
        set
        {
            _rotate = value;
        }
    }

    public void Rotate(int i)
    {
        _action?.Invoke(Rot);
    }
}
